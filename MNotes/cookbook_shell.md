# **`shell cookbook`**

@[shell, cookbook]

[TOC]

## 解压缩某个特定文件或目录
```shell
unzip <target-zip-file> '<folder-to-extract/*>' -d <destination-path> 
```

## 解决mac iTerm2终端中文乱码
**可能原因：**
MacOSX下默认的是utf8字符集，而默认的.zshrc没有设置为utf-8编码。可以通过输入`locale`查看字符编码设置情况（可以看到`LC_ALL=`是空的）。

**解决方案**
1.在终端下输入 
`vim ~/.zshrc`
2.在文件内容末端添加： 
```
export LC_ALL=en_US.UTF-8 
export LANG=en_US.UTF-8
```
接着重启一下终端，或者输入source ~/.zshrc使设置生效。 

**参考资料**
[iTerm2连接远程，中文乱码](https://blog.csdn.net/u013931660/article/details/79443037 )


## 查看磁盘空间和目录大小
`du -hs ./ownCloud` #查看ownCloud目录占用空间

`df -h` #df -H 查看硬盘剩余空间

## 合并文件
用提取工具提数时，如果结果过多，下载csv时会得到一个压缩包，内有分割后的很多个csv文件。这时需要将多个文件合并为一个。
```
find . | xargs cat >> path/out.csv
find . | grep csv |xargs cat >> path/out.csv
```


## 将用户加入root用户组
`usermod -G root yaolin`

`sudo chown -R yaolin software` #将目录software的拥有者改为用户yaolin
`chgrp -R yaolin software` #将目录software的用户组改为组yaolin

------

## 用户账号管理
``` powershell
#修改密码
sudo passwd root

#新建用户
adduser yaolin
#修改密码
passwd yaolin

#赋予root权限
#修改 /etc/sudoers 文件，找到下面一行，在root下面添加一行，如下所示：
## Allow root to run any commands anywhere
#root    ALL=(ALL)     ALL
#yaolin   ALL=(ALL)     ALL
#注：这个文件是只读的，不加“!”保存会失败。
vim /etc/sudoers
```

## linux环境配置
``` powershell
#安装tmux
sudo yum install tmux

#安装zsh
[Ubuntu 下安装oh-my-zsh](https://www.jianshu.com/p/9a5c4cb0452d)

```
[Ubuntu 下安装oh-my-zsh](https://www.jianshu.com/p/9a5c4cb0452d)
[centos 7 安装zsh](https://blog.csdn.net/qq_36373262/article/details/79206359)

## shell实现SSH自动登陆
[shell实现SSH自动登陆](https://www.cnblogs.com/zhenbianshu/p/5867440.html)

------

## 用sed删除第一行、最后一行或增加删除某行
``` powershell
#删除文档的第一行(直接对文本进行操作)
sed -i '1d' <file> 

#删除文档的第一行(只是看一下操作结果，不修改文本)
sed -e '1d' <file> 

#删除文档的最后一行
sed -i '$d' <file>

#删除文档的第1到6行
sed -i '1,6d' <file>

#删除文件中包含某个关键字开头的所有行
sed -i '/^QWQ/d' <file>

#删除文件中包含某个关键字的所有行 
sed -i '/QWQ/d' <file>

#在文档指定行中增加一行
sed -i '/* /a*' <file>
```